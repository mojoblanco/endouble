<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class SpaceDataTest extends TestCase
{
    /**
     * Test year 2013 and limit 2
     *
     * @return void
     */
    public function test_space_it_returns_bad_request_status_if_parameter_is_missing()
    {
        $request = $this->json('GET', '/api/data', [
            'sourceId' => 'space',
            'limit' => 1,
        ]);

        $request->assertStatus(400);
    }

     /**
     * Test year 2013 and limit 2
     *
     * @return void
     */
    public function test_space_it_returns_success_for_year_2013_and_limit_2()
    {
        $expectedJson = [
            'meta' => [
                'request' => [
                    'sourceId' => 'space',
                    'year' => 2013,
                    'limit' => 2,
                ],
                'timestamp' => '2018-08-21T18:30:00.000Z',
            ],
            'data' => [
                0 => [
                    'number' => 10,
                    'date' => '2013-03-01',
                    'name' => 'CRS-2',
                    'link' => 'https://en.wikipedia.org/wiki/SpaceX_CRS-2',
                    'details' => 'Last launch of the original Falcon 9 v1.0 launch vehicle',
                ],
                1 => [
                    'number' => 11,
                    'date' => '2013-09-29',
                    'name' => 'CASSIOPE',
                    'link' => 'http://www.parabolicarc.com/2013/09/29/falcon-9-launch-payloads-orbit-vandenberg/',
                    'details' => 'Commercial mission and first Falcon 9 v1.1 flight, with improved 13-tonne to LEO capacity. Following second-stage separation from the first stage, an attempt was made to perform an ocean touchdown test of the discarded booster vehicle. The test provided good test data on the experiment-its primary objective-but as the booster neared the ocean, aerodynamic forces caused an uncontrollable roll. The center engine, depleted of fuel by centrifugal force, shut down resulting in the impact and destruction of the vehicle.',
                ],
            ],
        ];

        $request = $this->json('GET', '/api/data', [
            'sourceId' => 'space',
            'year' => 2013,
            'limit' => 2,
        ]);

        $request->assertStatus(200)
            ->assertExactJson($expectedJson);
    }

    /**
     * Test year 2017 and limit 3
     *
     * @return void
     */
    public function test_space_it_returns_success_for_year_2017_and_limit_3()
    {
        $expectedJson = [
            'meta' => [
                'request' => [
                    'sourceId' => 'space',
                    'year' => 2017,
                    'limit' => 3,
                ],
                'timestamp' => '2018-08-21T18:30:00.000Z',
            ],
            'data' => [
                0 => [
                    'number' => 35,
                    'date' => '2017-01-14',
                    'name' => 'Iridium NEXT Mission 1',
                    'link' => 'https://spaceflightnow.com/2017/01/14/spacex-resumes-flights-with-on-target-launch-for-iridium/',
                    'details' => 'Return-to-flight mission after the loss of Amos-6 in September 2016. Iridium NEXT will replace the original Iridium constellation, launched in the late 1990s. Each Falcon mission will carry 10 satellites, with a goal to complete deployment of the 66 plus 9 spare satellite constellation by mid 2018. The first two Iridium qualification units were supposed to ride a Dnepr rocket in April 2016 but were delayed, so Iridium decided to qualify the first batch of 10 satellites instead.',
                ],
                1 => [
                    'number' => 36,
                    'date' => '2017-02-19',
                    'name' => 'CRS-10',
                    'link' => 'https://spaceflightnow.com/2017/02/19/historic-launch-pad-back-in-service-with-thundering-blastoff-by-spacex/',
                    'details' => 'First Falcon 9 flight from the historic LC-39A launchpad at Kennedy Space Center, carrying supplies and materials to support dozens of science and research investigations scheduled during ISS Expeditions 50 and 51. The first stage returned to launch site and landed at LZ-1.',
                ],
                2 => [
                    'number' => 37,
                    'date' => '2017-03-16',
                    'name' => 'EchoStar 23',
                    'link' => 'http://spacenews.com/spacex-launches-echostar-23/',
                    'details' => 'Communications satellite for EchoStar Corp. EchoStar XXIII, based on a spare platform from the cancelled CMBStar 1 satellite program, will provide direct-to-home television broadcast services over Brazil. There was no attempt at a first-stage recovery so this rocket did not have landing legs or grid fins.',
                ],
            ],
        ];

        $request = $this->json('GET', '/api/data', [
            'sourceId' => 'space',
            'year' => 2017,
            'limit' => 3,
        ]);

        $request->assertStatus(200)
            ->assertExactJson($expectedJson);
    }

}

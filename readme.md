# Endouble Test

## Prerequisite

- composer
- php 7.1+

## Installation

1. Clone the repository and navigate to the project folder
2. Make a copy of the environment file by running `cp env.example .env`
3. Install dependencies: `composer install`
4. Generate key: `php artisan key:generate`
5. Start the application: `php artisan serve`

## Usage
To view response from a source, open a web browser or Postman and navigate to the url `localhost:8000/api/data` and pass the following as query parameters

- sourceId: *string(e.g comics)*

- year: *integer(e.g 2018)*

- limit: *integer(e.g 3)*

For Example: `localhost:8000/api/data?sourceId=space&year=2018&limit=1`

*Make sure to pass a header of `Accept: application/json`*

## Run tests
To run all tests, use the folloing command `./vendor/bin/phpunit`


##  Adding a new resource
Adding a new resource is as easy as running an artisan command.

For example if you want a new resource for github, the followinc command is run;

`php artisan make:a data-source GithubResourceService`

This generates a file in the `App\Services` folder

Then you can now go to 'app/config/resource.php' and add a new entry which contains sourceId as key and the path to the generated file as value.
E.g
````php
    'github' => [
        'class' => 'App\Services\GithubResourceService'
    ],
````

The logic for gettig data is then put in the `getData` method

## Extra
In order to avoid abusing the api calls, caching has been enabled.

The default cache driver is `file` and this requries no extra setup

The default duration for the caching is 1 hour and the value can be changed by updating the duration key in the cache config file.
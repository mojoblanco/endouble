<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class ComicsDataTest extends TestCase
{
    /**
     * Test year 2015 and limit 2
     *
     * @return void
     */
    public function test_comics_it_returns_success_for_year_2015_and_limit_2()
    {
        $expectedJson = [
            'meta' => [
                'request' => [
                    'sourceId' => 'comics',
                    'year' => 2015,
                    'limit' => 2,
                ],
                'timestamp' => '2018-08-21T18:30:00.000Z',
            ],
            'data' => [
                0 => [
                    'number' => 1468,
                    'date' => '2015-01-02',
                    'name' => 'Worrying',
                    'link' => 'https://imgs.xkcd.com/comics/worrying.png',
                    'details' => 'If the breaking news is about an event at a hospital or a lab, move it all the way over to the right.',
                ],
                1 => [
                    'number' => 1469,
                    'date' => '2015-01-05',
                    'name' => 'UV',
                    'link' => 'https://imgs.xkcd.com/comics/uv.png',
                    'details' => 'Hey, why stop at our house? We could burn down ALL these houses for the insurance money.',
                ],
            ],
        ];

        $response = $this->json('GET', '/api/data', [
            'sourceId' => 'comics',
            'year' => 2015,
            'limit' => 2,
        ]);

        $response
            ->assertStatus(200)
            ->assertExactJson($expectedJson);
    }

    /**
     * Test year 2019 and limit 1
     *
     * @return void
     */
    public function test_comics_it_returns_success_for_year_2019_and_limit_1()
    {
        $expectedJson = [
            'meta' => [
                'request' => [
                    'sourceId' => 'comics',
                    'year' => 2019,
                    'limit' => 1,
                ],
                'timestamp' => '2018-08-21T18:30:00.000Z',
            ],
            'data' => [
                0 => [
                    'number' => 2093,
                    'date' => '2019-01-02',
                    'name' => 'Reminders',
                    'link' => 'https://imgs.xkcd.com/comics/reminders.png',
                    'details' => "The good news is that if the number of work and friend relationships you have exceeds your willingness to do the bare minimum to keep up with everyone's life events and stuff, one way or another that problem eventually solves itself.",
                ]
            ],
        ];

        $response = $this->json('GET', '/api/data', [
            'sourceId' => 'comics',
            'year' => 2019,
            'limit' => 1,
        ]);

        $response->assertStatus(200)
            ->assertExactJson($expectedJson);
    }
}

<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class HelpersTest extends TestCase
{
    /**
     * yearFromIso helper test
     *
     * @return void
     */
    public function test_when_iso_date_is_given_year_is_returned()
    {
        $year = yearFromIso('2019-08-10');

        $this->assertEquals($year, 2019);
    }
}

<?php

namespace App\Services;

use Cache;

class SpaceResourceService implements ResourceService
{
    /**
     * Spacex api url
     */
    private const API_URL = 'https://api.spacexdata.com/v2/launches';


    /**
     * Gets data from api call
     *
     * @param array requestData
     * @return array $result
     */
    public function getData(array $requestData)
    {
        $cacheKey = $requestData['sourceId'] . '-' . $requestData['year'] . '-' . $requestData['limit'];

        if (Cache::has($cacheKey)) {
            return Cache::get($cacheKey);
        }

        $data = $this->getApiData();

        if ($data['status'] == false) {
            abort(400, $data['message']);
        }

        $launches = [];

        foreach ($data['message'] as $launch) {
            if ($launch->launch_year == (string) $requestData['year']) {
                $launches[] = $launch;

                if (count($launches) ==  $requestData['limit']) {
                    break;
                }
            } else {
                continue;
            }
        }

        $result = $this->sortResult($launches);
        \Cache::put($cacheKey, $result, config('cache.duration')); // Cache data

        return $result;
    }

    /**
     * Sort result from api data.
     *
     * @param array $launches
     * @return array
     */
    private function sortResult($launches)
    {
        $result = [];

        foreach ($launches as $launch) {
            $result[] = [
                'number' => $launch->flight_number,
                'date' => date('Y-m-d', strtotime($launch->launch_date_utc)),
                'name' => $launch->mission_name,
                'link' => $launch->links->article_link,
                'details' => $launch->details,
            ];
        }

        return $result;
    }


    /**
     * Get data from api url.
     *
     * @return array
     */
    private function getApiData()
    {
        $client = new \GuzzleHttp\Client(['verify' => false]); // false is used to bypass SSL issue

        try {
            $response = $client->get(self::API_URL);

            return [
                'status' => true,
                'message' => json_decode($response->getBody()),
            ];
        } catch (\GuzzleHttp\Exception\GuzzleException $e) {
            \Log::error($e);
            return [
                'status' => false,
                'message' => 'Something went wrong',
            ];
        }
    }
}
<?php

namespace App\Http\Controllers\Api;

use App;
use Illuminate\Http\Request;
use App\Services\ResourceService;
use App\Http\Controllers\Controller;
use App\Http\Requests\ResourceRequest;
use App\Http\Controllers\Api\ResourceData;

use App\Services\DataSourceService;

class DataSourceController extends Controller
{

    /**
     * Search corresponding resource for data
     * based on request.
     *
     * @param ResourceRequest $request
     * @return Response
     */
    public function index(ResourceRequest $request)
    {
        $requestData['sourceId'] = $request->sourceId;
        $requestData['year'] = (int) $request->year;
        $requestData['limit'] = (int) $request->limit;

        $this->bindService($request->sourceId); // Binding Service to container

        $service = \App::make('App\Services\ResourceService'); // Resolving from container
        $result = $service->getData($requestData);

        return response()->json([
            'meta' => [
                'request' => $requestData,
                'timestamp' => '2018-08-21T18:30:00.000Z',
            ],
            'data' => $result
        ]);
    }

    /**
     * Get service based on source id
     *
     * @param string sourceId
     * @return void
     */
    public function bindService(string $sourceId)
    {
        $resourceConfig = 'resources.' . $sourceId . '.class';
        $resourceClass = config($resourceConfig);

        if(!$resourceClass) {
            abort(400, "Unable to find resource");
        }

        \App::bind('App\Services\ResourceService', $resourceClass);
    }
}

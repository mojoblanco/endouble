<?php

/**
 * Get year from ISO Date.
 *
 * @param string $date
 * @return string
 */
function yearFromIso(string $date)
{
    $year = trim(explode('-', $date)[0]);

    return $year;
}

/**
 * Get comic Id from comic link.
 *
 * @param string $link
 * @return string
 */
function comicIdFromLink(string $link)
{
    return trim(explode('/', $link)[1]);
}

/**
 * Format month and/or year.
 *
 * @param string $value
 * @return string
 */
function sanitizeDate(string $value)
{
    return (strlen($value) == 2) ? $value : '0' . $value;
}
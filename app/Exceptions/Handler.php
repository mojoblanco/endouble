<?php

namespace App\Exceptions;

use Log;
use Exception;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that are not reported.
     *
     * @var array
     */
    protected $dontReport = [
        //
    ];

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var array
     */
    protected $dontFlash = [
        'password',
        'password_confirmation',
    ];

    /**
     * Status code to be returned.
     *
     * @var array
     */
    protected $statusCode;

    /**
     * Error message to be returned.
     *
     * @var array
     */
    protected $errorMessage;

    /**
     * Report or log an exception.
     *
     * @param  \Exception  $exception
     * @return void
     */
    public function report(Exception $exception)
    {
        parent::report($exception);
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Exception  $exception
     * @return \Illuminate\Http\Response
     */
    public function render($request, Exception $exception)
    {
        $this->statusCode = 400; // Set default to bad request code
        Log::error($exception); // Log exception

        if ($request->isJson() || $request->wantsJson()) {
            // If the app is in debug mode
            if (config('app.debug')) {
                $this->errorMessage = $exception->getMessage();
            }

            if ($exception instanceof \Illuminate\Validation\ValidationException) {
                return response()->json([
                    'meta' => [
                        'request' => $request->query(),
                        'timestamp' => date('Y-m-d\TH:i:s.sZ\Z'),
                    ],
                    'data' => [],
                    'error' => $exception->errors(),
                ], $this->statusCode);
            }

            // Use Exception code if instance of HttpException
            if ($this->isHttpException($exception)) {
                $this->statusCode = $exception->getStatusCode();
            }

            $message = $this->errorMessage ?? 'Ooops! Something went wrong!';

            // Return a JSON response with the response array and status code
            return response()->json([
                'meta' => [
                    'request' => $request->query(),
                    'timestamp' => date('Y-m-d\TH:i:s.sZ\Z'),
                ],
                'data' => [],
                'error' => [
                    'message' => [$message],
                ],
            ], $this->statusCode);
        }

        return parent::render($request, $exception);
    }
}

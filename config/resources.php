<?php

return [
    'space' => [
        'class' => 'App\Services\SpaceResourceService'
    ],
    'comics' => [
        'class' => 'App\Services\ComicsResourceService'
    ],
];
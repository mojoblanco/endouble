<?php

namespace App\Services;

use Cache;

class ComicsResourceService implements ResourceService
{
    /**
     * xkcd api url
     */
    private const API_URL = 'https://xkcd.com';

    /**
     * xkcd comics wiki url
     */
    private const WIKI_URL = 'http://www.explainxkcd.com/wiki/index.php/List_of_all_comics_(full)';

    /**
     * Gets data from api call
     *
     * @param array requestData
     * @return array $result
     */
    public function getData(array $requestData)
    {
        $cacheKey = $requestData['sourceId'] . '-' . $requestData['year'] . '-' . $requestData['limit'];

        if (Cache::has($cacheKey)) {
            return Cache::get($cacheKey);
        }

        $result = [];
        $data = file_get_contents(self::WIKI_URL);

        $dom = new \DOMDocument();
        @$dom->loadHTML($data);
        $tags = $dom->getElementsByTagName('tr');
        $xpath = new \DOMXPath($dom);

        for ($node = $tags->length - 1; $node >= 0; --$node) {
            if ($node == 0)
                continue;

            $comicDate = $xpath->query(".//td[@data-sort-type='isoDate']", $tags[$node])[0]->nodeValue;
            $comicUrl = $xpath->query(".//td[@data-sort-value]", $tags[$node])[0]->nodeValue;

            $year = yearFromIso($comicDate);
            $comicId = comicIdFromLink($comicUrl);

            if ($year != $requestData['year'])
                continue;

            $result[] = $this->getComicsData($comicId);

            if (count($result) == $requestData['limit']) {
                break;
            }
        }

        \Cache::put($cacheKey, $result, config('cache.duration')); // Cache data
        return $result;
    }


     /**
     * Get comic data from comic url.
     *
     * @param int $comicId
     * @return array
     */
    private function getComicsData(int $comicId)
    {
        $comicUrl = self::API_URL . "/{$comicId}/info.0.json";

        $content = json_decode(file_get_contents($comicUrl));

        return [
            'number' => $content->num,
            'date' => $content->year . '-' . sanitizeDate($content->month) . '-' .sanitizeDate($content->day),
            'name' => $content->title,
            'link' => $content->img,
            'details' => $content->alt,
        ];
    }
}
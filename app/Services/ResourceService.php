<?php

namespace App\Services;

interface ResourceService
{
    /**
     * Gets data from api call
     *
     * @param array requestData
     */
    public function getData(array $requestData);
}